**IRAF Image Reduction Practice Set for NGC 7281**

This repository contains basic data to practice reduction of image files using IRAF.

Get the companion set *sasiraf* prior to using these data. The 
*sasiraf* repository has the description and files needed to support
reduction within this project. Details are maintained there.


## Preliminary Steps (Do This Once on your Machine)

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Make a directory ~/Observations
2. cd ~/Observations


---

## Obtain a Observing project (This one is called ngc7281)

The basic idea of these datasets is to contain the raw data, planning and calibration
images related to the project.

1. Get (git) the directory 
2. The raw directory contains the images as they come from the telescope.
..* These files likely will contain spaces and other characters that IRAF in particular and Unix in general
does not like.

